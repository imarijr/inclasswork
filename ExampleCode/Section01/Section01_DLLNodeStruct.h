/*************************************
 * File name: Section01_DLLNodeStruct.h 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * Contains the struct for a Node for a Doubly
 * Linked List 
 * ***********************************/

#ifndef SECTION01_DLLNODESTRUCT_H
#define SECTION01_DLLNODESTRUCT_H

template<class T>
struct Node {
    T    data;
    Node *next;
    Node *prev;
};

#endif
